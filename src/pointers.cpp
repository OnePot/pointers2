#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;
  
  /* a is a pointer storing address of x
	 b is a pointer storing address of x
	 c is a variable storing the data in the address 'a'
	 d is a variable stroing 2 x data in b 
	  ^b pointing the address of x, data in x = 25
   */


  cout<<"x = "<<x<<", y = "<<y<<endl;
  cout<< *b << "  " << d;

 



}
